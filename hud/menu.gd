extends Control


func _on_start_pressed():
	await get_parent().toggle_menu()
	get_parent().go_to_next_segment()


func open():
	show()
	%ControllerTransitions.play("open_menu")


func close():
	%ControllerTransitions.play("close_menu")
	await %ControllerTransitions.animation_finished
	hide()
