extends Control

@export var show_scene : PackedScene
@export var game_segments : StringList

var minigame_scenes: Dictionary = {
	"familiar_cats": preload("res://minigames/familiar_cats/FamiliarCats.tscn"),
	"abracajumble": preload("res://minigames/abracajumble/Abracajumble.tscn"),
	"sweepers": preload("res://minigames/stop_those_sweepers/Sweepers.tscn"),
	"tricky_trivia": preload("res://minigames/tricky_trivia/TrickyTrivia.tscn"),
}

var current_scene : Node
var remaining_segments : Array


func _ready():
	remaining_segments = game_segments.list.duplicate()
	toggle_menu()


func go_to_next_segment(previous_segment : String = ""):
	var regex = RegEx.new()
	regex.compile("game_over")
	if regex.search(previous_segment): end_game()
	else:
		var next_segment = remaining_segments.pop_front()
		if minigame_scenes.keys().find(next_segment) != -1: go_to_minigame(next_segment)
		elif next_segment == "wheel_of_wails": go_to_wheel()
		else: go_to_show(next_segment)


func end_game():
	Match.reset_match()
	_ready()
	$WheelOfWails._ready()


func toggle_menu(): #ALL WRONG!!
	if $Menu.visible:
		$Menu.close()
		%TVTransitions.play("from_show_to_menu")
		await %TVTransitions.animation_finished
		if current_scene: current_scene.show()
	else:
		$Menu.open()
		if current_scene: current_scene.hide()
		%TVTransitions.play("from_menu_to_show")
	

func go_to_show(segment):
	var show_segment = show_scene.instantiate()
	show_segment.segment_ended.connect(go_to_next_segment)
	show_segment.dialogue_content = load("res://assets/scripts/" + segment + ".tres")
	await _change_to_scene(show_segment)
	show_segment.start()
	$TV.show()


func go_to_minigame(minigame_name):
	_change_to_scene(minigame_scenes[minigame_name].instantiate())
	$TV.hide()


func go_to_wheel():
	$WheelOfWails.show()
	$TV.hide()


func _change_to_scene(scene: Node):
	if current_scene:
		current_scene.queue_free()
		await current_scene.tree_exited
	current_scene = scene
	add_child(scene)
	return


func _input(event):
	if event.is_action_pressed("ui_menu"):
		toggle_menu()
