extends Node


func _ready():
	randomize()


func pick_letter(letters : Array, current_backfacing_slots : Array):
	await get_tree().create_timer(1).timeout
	
	var available_letters = letters.filter(func(letter): return not letter.disabled)
	
	var picked_letter : Button
	if len(current_backfacing_slots) >= 1 and len(current_backfacing_slots) <= 3:
		for letter in available_letters:
			if letter.text == current_backfacing_slots[0].text:
				picked_letter = letter
	elif len(available_letters) > 0:
		picked_letter = available_letters.pick_random()
	
	if picked_letter: picked_letter.pick()
		
