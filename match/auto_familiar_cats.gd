extends Node

var cat_set : Array = []
var fast_search : float = 1.65
var slow_search : float = 4.0


func _ready():
	randomize()


func pick_cat(target_set : Array, game : Control):
	var matching_cat : Texture2D
	
	for cat in target_set:
		if cat_set.find(cat):
			matching_cat = cat
	
	if get_parent().score.familiar_cats >= 4:
		fast_search = 0.8
		slow_search = 3
	
	await get_tree().create_timer(randf_range(fast_search, slow_search)).timeout
	if get_parent().playing and target_set == game.target_set:
		game.validate_pick(matching_cat, get_parent())
