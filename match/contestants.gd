extends Node

@export var contestant_name : String = ""
@export var abracajumble : Node
@export var familiar_cats : Node
@export var sweepers : Node

var score : Dictionary = {
	"abracajumble": 0,
	"familiar_cats": 0,
	"sweepers": 0
}

var playing = false
