extends Node

@export var player_character : Node

@onready var active_contestants = get_children()

var bonuses : Dictionary = {
	"lifeline": false,
	"extra_time": false,
}


func reset_match():
	active_contestants = get_children()
	
	for contestant in active_contestants:
		for game in contestant.score.keys():
			contestant.score[game] = 0
	
	bonuses = { "lifeline": false, "extra_time": false }


func reset_minigame_score(game_name : String):
	for contestant in active_contestants:
		contestant.score[game_name] = 0
	
