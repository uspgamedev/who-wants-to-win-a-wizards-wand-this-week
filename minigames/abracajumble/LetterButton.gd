extends Button

signal picked(letter)


func _on_pressed():
	pick()

func pick():
	picked.emit(text)
	disabled = true
