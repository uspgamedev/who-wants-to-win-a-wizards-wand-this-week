extends MiniGame

const ALPHABET = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
const LETTER_BUTTON : PackedScene = preload("res://minigames/abracajumble/LetterButton.tscn")

@export var game_content : JSON

var level : int = 0
var target_word_set : Dictionary
var categories : Array = ["WIZARD TOOLS AND SYMBOLS", "FAMOUS WIZARD QUOTES", "WIZARD POWERS AND ABILITIES", "MUSICAL WIZARDS"]
var current_contestant : Node = Match.player_character
var current_contestant_index : int = 0
var current_backfacing_slots : Array = []

@onready var word_sets: Dictionary = game_content.data



func setup_game():
	update_target_words(get_random_word_set())
	initialize_letterboard()


func update_target_words(new_set):
	target_word_set = new_set
	
	%Category.text = "CATEGORY: " + categories[level]
	%Clue.text = "CLUE: " + target_word_set["clue"]
	
	clear_target_words()
	
	var words = target_word_set["answer"].rsplit(" ")
	var word_slots = %TargetWords.get_children()
	
	for word_slot_index in range(len(word_slots)):
		if word_slot_index <= len(words) - 1:
			var letter_slots = word_slots[word_slot_index].get_children()
			var letters = words[word_slot_index].rsplit()
			
			for letter_slot_index in range(len(letter_slots)):
				var slot = letter_slots[letter_slot_index]
				
				if letter_slot_index <= len(letters) - 1:
					var text = letters[letter_slot_index]
					
					slot.text = text
					
					# Check if it's a letter or a symbol
					var regex = RegEx.new()
					regex.compile("[A-Z]+")
					if regex.search(str(text)):
						slot.face_back()
						current_backfacing_slots.append(slot)
					else:
						slot.face_front()
				else:
					slot.text = ""
					slot.omit()
			word_slots[word_slot_index].show()
		else:
			word_slots[word_slot_index].hide()


func initialize_letterboard():
	for letter in ALPHABET:
		var new_button = LETTER_BUTTON.instantiate()
		new_button.picked.connect(validate_pick)
		new_button.text = letter
		$Letterboard.add_child(new_button)


func get_random_word_set():
	return word_sets[categories[level]].pick_random()


func show_clue():
	pass


func hide_clue():
	pass


func clear_target_words():
	for word_slot in %TargetWords.get_children():
		for letter_slot in word_slot.get_children():
			letter_slot.text = ""


func validate_pick(letter_picked):
	var correct_pick := false
	
	for word in %TargetWords.get_children():
		for letter in word.get_children():
			if letter.text == letter_picked:
				letter.face_front()
				current_backfacing_slots.erase(letter)
				correct_pick = true
	
	if correct_pick:
		current_contestant.score[game_name] += 1
		check_solve()
		if not current_contestant == Match.player_character: 
			current_contestant.abracajumble.pick_letter($Letterboard.get_children(), current_backfacing_slots)
	else:
		end_turn()


func check_solve():
	for word in %TargetWords.get_children():
		for letter in word.get_children():
			if not letter.omitted and not letter.front_facing: return
	level_up()


func level_up():
	level += 1
	current_backfacing_slots.clear()
	if level > len(categories) - 1: show_results()
	else:
		await get_tree().create_timer(0.75).timeout
		update_target_words(get_random_word_set())
		get_tree().call_group("LETTER BUTTON", "set_disabled", false)


func end_turn():
	if current_contestant_index < len(Match.active_contestants) - 1:
		current_contestant_index += 1
	else: current_contestant_index = 0
	
	current_contestant = Match.active_contestants[current_contestant_index]
	
	if current_contestant == Match.player_character:
		$Letterboard.show()
	else:
		current_contestant.abracajumble.pick_letter($Letterboard.get_children(), current_backfacing_slots)
		$Letterboard.hide()
