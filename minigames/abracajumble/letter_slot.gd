extends Label

var front_stylebox = preload("res://assets/abracajumble/FrontLetterSlot.tres")
var back_stylebox = preload("res://assets/abracajumble/BackLetterSlot.tres")

var front_facing : bool = false
var omitted : bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	omit()


func face_front():
	add_theme_stylebox_override("normal", front_stylebox)
	remove_theme_color_override("font_color")
	front_facing = true
	omitted = false


func face_back():
	add_theme_stylebox_override("normal", back_stylebox)
	add_theme_color_override("font_color", Color(0, 0, 0, 0))
	front_facing = false
	omitted = false


func omit():
	remove_theme_stylebox_override("normal")
	add_theme_color_override("font_color", Color(0, 0, 0, 0))
	omitted = true
