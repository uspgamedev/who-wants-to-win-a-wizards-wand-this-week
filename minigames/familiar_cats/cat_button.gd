extends TextureButton

signal picked(cat_texture, origin)


func _on_pressed():
	picked.emit(texture_normal, self)
