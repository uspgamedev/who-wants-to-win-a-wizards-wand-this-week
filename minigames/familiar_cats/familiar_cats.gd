extends MiniGame

const CAT_BUTTON : PackedScene = preload("res://minigames/familiar_cats/CatButton.tscn")

@export var number_of_cats: int = 37
@export var cats_per_set: int = 9
@export var key_intervals: Array = [0, 1, 3, 8, 12, 14, 17, 18, 30]

var cat_list : Array
var set_list : Array
var target_set: Array



# Creates a list of cat pictures
func initialize_cat_list():
	for cat_index in range(number_of_cats):
		var cat_picture = load("res://assets/familiar_cats/emoji/" + str(cat_index) + ".png")
		cat_list.append(cat_picture)
	

# Creates a list of sets of cats so that each pair of cat sets will only have 2 matching cats
func initialize_set_list():
	# Creates list of empty sets
	for n in range(number_of_cats):
		var new_set : Array = []
		set_list.append(new_set)
	
	# Calculates the sets in which each cat should be featured and adds the cat to those sets
	for cat_index in number_of_cats:
		for interval in key_intervals:
			var set_index = (interval + cat_index) % number_of_cats
			set_list[set_index].append(cat_list[cat_index])


# Creates as many cat buttons to be picked by the player as defined in cats per set
func initialize_player_buttons():
	for n in range(cats_per_set):
		var new_button = CAT_BUTTON.instantiate()
		new_button.picked.connect(validate_pick)
		$CurrentCats.add_child(new_button)


# Picks a new random set of cats and makes it unavailable to be picked in the future
func get_random_set():
	var random_set = set_list.pick_random()
	set_list.erase(random_set)
	
	return random_set


func showcase():
	while dialogue_manager.running:
		await get_tree().create_timer(0.5).timeout
		if dialogue_manager.running: update_target_set(set_list.pick_random())


# Updates set of cats the player has to match to a given set of cats
func update_target_set(new_set):
	target_set = new_set
	
	var spots_list = $TargetCats.get_children()
	
	for spot in spots_list:
		spot.texture = null
	
	var available_spots : Array = spots_list.duplicate()
	
	for cat in target_set:
		var spot = available_spots.pick_random()
		spot.texture = cat
		available_spots.erase(spot)


func start_autoplay():
	for contestant in Match.active_contestants:
		if contestant != Match.player_character:
			contestant.familiar_cats.pick_cat(target_set, self)


# Updates set of cats the player can choose from to a given set of cats
func update_player_buttons():
	var button_list = $CurrentCats.get_children()
	for i in range(cats_per_set):
		button_list[i].texture_normal = Match.player_character.familiar_cats.cat_set[i]


func validate_pick(picked_cat : Texture2D, origin : Node):
	var picker = origin if Match.active_contestants.find(origin) != -1 else Match.player_character
	
	if target_set.find(picked_cat) != -1 and not dialogue_manager.running:
		picker.score[game_name] += 1
		if set_list.is_empty():
			show_results()
			for contestant in Match.active_contestants:
				contestant.playing = false
		else: start_new_round(picker)
	else:
		## TODO: implement error feedback
		pass


func start_new_round(picker: Node = null):
	var new_target = picker.familiar_cats.cat_set if picker else get_random_set()
	if picker: picker.familiar_cats.cat_set = get_random_set()
	update_target_set(new_target)
	update_player_buttons()
	start_autoplay()


func pre_setup():
	initialize_cat_list()
	initialize_set_list()


func setup_game():
	initialize_player_buttons()
	
	for contestant in Match.active_contestants:
		contestant.familiar_cats.cat_set = get_random_set()
		contestant.playing = true
	
	start_new_round()
