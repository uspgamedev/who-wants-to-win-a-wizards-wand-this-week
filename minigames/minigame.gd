extends Control
class_name MiniGame

@export var dialogue_manager : DialogueManager
@export var game_name : String


func _ready():
	dialogue_manager.segment_ended.connect(_on_segment_ended)
	play_intro()
	
	randomize()
	pre_setup()
	showcase()


func play_intro():
	dialogue_manager.dialogue_content = load("res://assets/scripts/" + game_name + "_intro.tres")
	dialogue_manager.start()


func show_results():
	var loser = Match.player_character
	
	for contestant in Match.active_contestants:
		print(contestant.name, ": ", contestant.score[game_name])
		if contestant.score[game_name] < loser.score[game_name]: loser = contestant
		
	print("loser: ",loser)
	
	await get_tree().create_timer(1).timeout
	if loser == Match.player_character:
		if Match.bonuses.lifeline:
			Match.reset_minigame_score(game_name)
			Match.bonuses.lifeline = false
			pre_setup()
			setup_game()
			#TODO: add some sort of introduction dialogue before the reset
		else: get_parent().go_to_show("game_over")
	else:
		play_outro(loser)
		Match.active_contestants.erase(loser)


func play_outro(loser: Node):
	dialogue_manager.dialogue_content = load("res://assets/scripts/" + loser.contestant_name + "_outro.tres")
	dialogue_manager.start()


func pre_setup():
	pass


func showcase():
	pass


func setup_game():
	pass


func _on_segment_ended(segment: String):
	# Check if it's intro or outro
	var regex = RegEx.new()
	regex.compile("intro")
	if regex.search(segment): setup_game()
	else: get_parent().go_to_show(game_name + "_letter_reveal")
	
