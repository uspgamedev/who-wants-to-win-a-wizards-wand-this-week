extends CharacterBody2D

const SPEED = 50.0
const KNOCKBACK_STRENGTH = 600.0

var target : Node2D
var direction : Vector2 = Vector2.ZERO
var paused : bool = false

@onready var speed = SPEED



func _physics_process(_delta):
	if not paused:
		direction = position.direction_to(target.position)
		velocity = direction.normalized() * speed
		move_and_slide()


func knockback(source, intensifier):
	var knockback_direction = source.position.direction_to(position)
	var new_global_position = global_position + knockback_direction * KNOCKBACK_STRENGTH * intensifier
	
	global_position = global_position.lerp(new_global_position, .25)


func pause():
	paused = true
	

func unpause():
	paused = false
