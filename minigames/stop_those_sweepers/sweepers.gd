extends MiniGame

const BROOM : PackedScene = preload("res://minigames/stop_those_sweepers/Broom.tscn")

@export var well : Node2D
@export var defender : Node2D
@export var whack_area : Area2D

var in_control: bool = true


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if in_control: $RotatingDefender.look_at(get_global_mouse_position())
	if not dialogue_manager.running:
		Match.player_character.score[game_name] += 1 * delta


func pre_setup():
	get_tree().call_group("BROOM", "queue_free")
	for i in range(5):
		var new_broom = BROOM.instantiate()
		var x_position = randi_range(0,340) if i <= 2 else randi_range(810,1152)
		var y_position = randi_range(0,90) if i >= 2 else randi_range(560,1129)
		new_broom.position = Vector2(x_position, y_position)
		new_broom.target = well
		add_child(new_broom)


func showcase():
	get_tree().call_group("BROOM", "pause")


func setup_game():
	get_tree().call_group("BROOM", "unpause")


func _on_gui_input(event):
	if event is InputEventMouseButton and event.button_index == 1 and not event.pressed:
		var brooms_in_area = whack_area.get_overlapping_bodies()
		if not brooms_in_area.is_empty():
			whack(brooms_in_area)


func whack(broom_list):
	for broom in broom_list:
		if broom.scale.x < 0.4: broom.queue_free()
		else:
			var new_broom = BROOM.instantiate()
			new_broom.position = broom.position + Vector2(10,10)
			new_broom.target = broom.target
			add_child(new_broom)
			
			var new_scale = broom.scale * 0.8
			broom.scale = new_scale
			new_broom.scale = new_scale
			
			var new_speed = broom.speed * 1.2
			broom.speed = new_speed
			new_broom.speed = new_speed
			
			broom.knockback(defender, len(broom_list))
			new_broom.knockback(defender, len(broom_list))


func _on_well_body_entered(body):
	if body.is_in_group("BROOM"):
		body.queue_free()
		if not $Results.visible:
			for contestant in Match.active_contestants:
				if contestant != Match.player_character: contestant.sweepers.calculate_score()
				
			show_results()
			in_control = false
