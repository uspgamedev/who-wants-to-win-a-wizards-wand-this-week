extends MiniGame

const RIGHT_OPTION: Texture2D = preload("res://assets/tricky_trivia/option_panel_correct.png")
const WRONG_OPTION: Texture2D = preload("res://assets/tricky_trivia/option_panel_wrong.png")


@export var trivia_questions : JSON

var level : int = 0
var current_question : Dictionary
var chosen_option : String = ""
var dificulties : Array = ["TRIVIAL", "EASY", "MEDIUM", "HARD", "IMPOSSIBLE"]

@onready var option_labels : Dictionary = {
	"A": %OptionA, 
	"B": %OptionB, 
	"C": %OptionC, 
	"D": %OptionD
}
@onready var option_buttons : Dictionary = {
	"A": %OptionButtonA, 
	"B": %OptionButtonB, 
	"C": %OptionButtonC, 
	"D": %OptionButtonD
}
@onready var questions: Dictionary = trivia_questions.data



func _process(_delta):
	if not $QuestionTimeLimit.is_stopped(): $TimeLeft.text = str(int($QuestionTimeLimit.time_left))


func setup_game():
	if Match.bonuses.extra_time: $QuestionTimeLimit.wait_time += 30
	$TimeLeft.text = "60"
	#TODO: setup animation to show exra_time being consumed
	update_question(get_random_question())


func clear_option_labels():
	for option in option_labels.keys():
		option_labels[option].text = ""


func update_question(new_question):
	current_question = new_question
	
	%Question.text = current_question.question
	$UI.show()
	
	clear_option_labels()
	await get_tree().create_timer(1).timeout
	
	for option in option_labels.keys():
		await get_tree().create_timer(1).timeout
		option_labels[option].text = current_question[option]
	
	$QuestionTimeLimit.start()
	$TimeLeft.show()


func get_random_question():
	return questions[dificulties[level]].pick_random()


func validate_pick():
	option_buttons[chosen_option].disabled = true
	
	if chosen_option == current_question.answer:
		option_buttons[chosen_option].button_pressed = false
		option_buttons[chosen_option].texture_disabled = RIGHT_OPTION
		await get_tree().create_timer(1).timeout
		level_up()
	else: 
		option_buttons[chosen_option].texture_disabled = WRONG_OPTION
		await get_tree().create_timer(1).timeout
		get_parent().go_to_show("game_over")


func level_up():
	level += 1
	
	for option in option_labels.keys():
		option_labels[option].text = ""
		option_buttons[option].disabled = false
	
	if level > len(dificulties) - 1: get_parent().go_to_next_segment(game_name)
	else:
		$UI.hide()
		$TimeLeft.hide()
		await get_tree().create_timer(1).timeout
		update_question(get_random_question())


func update_selected_option():
	for option in option_buttons.keys():
		option_buttons[option].button_pressed = false
	option_buttons[chosen_option].button_pressed = true


func check_final_answer(clicked_option : String):
	if chosen_option == clicked_option:
		$QuestionTimeLimit.stop()
		$TimeLeft.hide()
		validate_pick()
	else:
		chosen_option = clicked_option
		update_selected_option()


func _on_option_button_a_pressed(): check_final_answer("A")
func _on_option_button_b_pressed(): check_final_answer("B")
func _on_option_button_c_pressed(): check_final_answer("C")
func _on_option_button_d_pressed(): check_final_answer("D")


func _on_question_time_limit_timeout():
	validate_pick()
