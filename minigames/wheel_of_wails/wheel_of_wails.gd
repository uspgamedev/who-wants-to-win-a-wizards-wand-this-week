extends Control


@export var wheel_options : StringList

const SPIN_SPEED = 360

var target_degrees : float = 0.0
var chosen_option : String = ""
var available_options : Array
var sent_to_option : bool = false

@onready var speed = SPIN_SPEED
 


func _ready():
	available_options = wheel_options.list.duplicate()
	available_options.erase("tricky_trivia")


func _process(delta):
	if target_degrees != 0: spin_wheel(delta)


func _on_gui_input(event):
	if event.is_action_pressed("advance") and target_degrees == 0:
		randomize()
		await pick_available_option()
		calculate_rotation()


func pick_available_option():
	if len(Match.active_contestants) == 1: available_options.append("tricky_trivia")
	if chosen_option == "": chosen_option = available_options.pick_random()
	
	available_options.erase(chosen_option)


func calculate_rotation():
	var start_rotation : float = $WheelSprite.rotation_degrees
	var previous_option_index : int = int(start_rotation / 45) % 8
	var chosen_option_index : int = wheel_options.list.find(chosen_option)
	var sections_to_spin : int = chosen_option_index - previous_option_index
	var extra_spins : int = randi_range(2, 4) * 8
	target_degrees = start_rotation + (sections_to_spin + extra_spins) * 45


func spin_wheel(delta):
	if $WheelSprite.rotation_degrees < target_degrees:
		$WheelSprite.rotation_degrees += speed * delta
		if $WheelSprite.rotation_degrees > target_degrees - 720: decelerate(delta)
	elif not sent_to_option:
		sent_to_option = true
		await get_tree().create_timer(1).timeout
		go_to_chosen_option()


func decelerate(delta):
	if speed > 10:
		speed -= 90 * delta


func go_to_chosen_option():
	if get_parent().minigame_scenes.keys().find(chosen_option) != -1:
		get_parent().go_to_minigame(chosen_option)
		hide()
	else: 
		$Explanations.dialogue_content = load("res://assets/scripts/" + chosen_option + ".tres")
		$Explanations.start()
		
		if Match.bonuses.keys().find(chosen_option) != -1:
			Match.bonuses[chosen_option] = true
		
		await $Explanations.segment_ended
		#TODO: implement bonuses reminder interface
	
	reset_wheel()

func reset_wheel():
	target_degrees = 0
	chosen_option = ""
	speed = SPIN_SPEED
	sent_to_option = false
	
