extends Control
class_name SpeechBubble

func update_info(type, text):
	clear()
	
	if type and text:
		var chosen_bubble = get_node(type)
		
		chosen_bubble.get_node("Speech").text = text
		chosen_bubble.show()

func clear():
	get_tree().call_group("BUBBLE", "hide")
	
