extends Control
class_name DialogueManager

signal segment_ended(segment)

@export var dialogue_content : JSON
@export var image_slot : TextureRect
@export var sfx_slot : AudioStreamPlayer
@export var speech_bubble : SpeechBubble

var current_dialogue_index : int = -1
var current_dialogue : Dictionary = {}
var running = false
var timer_running = false
var dialogue: Array



func start():
	show()
	running = true
	dialogue = dialogue_content.data
	current_dialogue_index = -1
	advance_dialogue()


func stop():
	hide()
	running = false
	current_dialogue_index = -1
	segment_ended.emit(dialogue_content.get_path())


func advance_dialogue():
	if current_dialogue_index < len(dialogue) - 1:
		current_dialogue_index += 1
		current_dialogue = dialogue[current_dialogue_index]
		update_dialogue()
	elif running: stop()


func update_dialogue():
	if speech_bubble:
		speech_bubble.update_info(current_dialogue.speech_bubble, current_dialogue.text)
	
	if image_slot and current_dialogue.image:
		image_slot.texture = load(current_dialogue.image) 
	
	if sfx_slot and current_dialogue.sfx:
		sfx_slot.stream = load(current_dialogue.sfx)
		$SFX.play()
	
	if current_dialogue.time:
		timer_running = true
		await get_tree().create_timer(current_dialogue.time).timeout
		if timer_running: advance_dialogue() #allows clicking to skip


func _on_gui_input(event):
	if event.is_action_pressed("advance"): 
		timer_running = false
		advance_dialogue()
